/**
 * Cogton is the HTML5 Canvas powered rendering of the 2013 Oddineers logo
 * Copyright 2014 Oddineers Ltd. Version 1.0.2
 */

/**
 * [Gear description]
 * @param {[type]} config [description]
 */
function Gear(config){
    this.x = config.x;
    this.y = config.y;
    this.outerRadius = config.outerRadius;
    this.innerRadius = config.innerRadius;
    this.holeRadius = config.holeRadius;
    this.numTeeth = config.numTeeth;
    this.theta = config.theta;
    this.thetaSpeed = config.thetaSpeed;
    this.lightColor = config.lightColor;
    this.darkColor = config.darkColor;
    this.clockwise = config.clockwise;
    this.midRadius = config.outerRadius - 10;
}
Gear.prototype.draw = function(context){
    //grab the height and width on the canvas element
    var anim = new Animation("myCanvas");
    var canvas = anim.getCanvas();

    // Make it workable
    var cw = (canvas.scrollWidth / 15);

    context.save();
    context.translate(this.x, this.y);
    context.rotate(this.theta);
    // draw gear teeth
    context.beginPath();
    // we can set the lineJoin property to bevel so that the tips of the gear teeth are flat and don't come to a sharp point
    context.lineJoin = "bevel";
    // loop through the number of points to create the gear shape factoring in: canvas width / 10
    var numPoints = this.numTeeth * Math.floor(cw / 10) ;
    for (var n = 0; n < numPoints; n++) {
        var radius = null;
        // draw tip of teeth on even iterations
        if (n % 2 == 0) {
            radius = this.outerRadius;
        }
        // draw teeth connection which lies somewhere between the gear center and gear radius
        else {
            radius = this.innerRadius;
        }
        
        var theta = ((Math.PI * 2) / numPoints) * (n + 1);
        var x = (radius * Math.sin(theta));
        var y = (radius * Math.cos(theta));
        // if first iteration, use moveTo() to position the drawing cursor
        if (n == 0) {
            context.moveTo(x, y);
        }
        // if any other iteration, use lineTo() to connect sub paths
        else {
            context.lineTo(x, y);
        }
    }
    
    context.closePath();
    // define the line width and stroke color
    context.lineWidth = 15;
    context.strokeStyle = this.darkColor;
    context.stroke();
    // draw gear body
    context.beginPath();
    context.arc(0, 0, this.midRadius, 0, 2 * Math.PI, false);
    
    // create a linear gradient
    var grd = context.createLinearGradient(-1 * this.outerRadius / 2, -1 * this.outerRadius / 2, this.outerRadius / 2, this.outerRadius / 2);
    
    grd.addColorStop(0, this.lightColor); 
    grd.addColorStop(1, this.darkColor); 
    context.fillStyle = grd;
    context.fill();

    /*Some shadow effects can be applied to each rendered line but.. */
    //context.shadowColor = '#999';
    //context.shadowBlur = 20;
    //context.shadowOffsetX = 0;
    //context.shadowOffsetY = 0;
    context.lineWidth = 3;
    context.strokeStyle = this.darkColor;
    context.stroke();
    
    // draw gear hole
    context.beginPath();

    context.arc(0, -cw, cw / 2, 0, 2 * Math.PI, false);
    context.arc(-cw, 0, cw / 2, 0, 2 * Math.PI, false);

    context.fillStyle = "#454950";
    context.fill();

    // close path
    context.closePath();
    
    // reopen path to draw again
    context.beginPath();

    context.arc(cw, 0, cw / 2, 0, 2 * Math.PI, false);
    context.arc(0, cw, cw / 2, 0, 2 * Math.PI, false);

    context.fillStyle = "#454950";
    context.fill();

    context.closePath();

    //context.strokeStyle = this.darkColor;
    //context.stroke();
    context.restore();
};

function cogton_animate(animated) {
    window.onload = function(){
        var anim = new Animation("myCanvas");
        var canvas = anim.getCanvas();
        var context = anim.getContext();
        var gears = [];
        
        // Determine the size of the Canvas and work form there
        var canvasId = document.getElementById('myCanvas');
        var canvasWidth = canvas.scrollWidth / 4;
        var canvasHeight = canvas.scrollHeight / 4;

        // 1st gear
        gears.push(new Gear({
            x: canvasWidth * 2,
            y: canvasHeight * 2,
            outerRadius: canvasWidth,           //90
            innerRadius: canvasWidth - 40,      //50
            holeRadius: 20,
            numTeeth: 16,
            theta: 0,
            thetaSpeed: 1 / 1000,
            lightColor: "#33363B", //"#eae7d5",
            darkColor: "#33363B", //"#eae7d5", or creme?
            clockwise: false
        }));
        // 2nd gear or rather button
        gears.push(new Gear({
            x: canvasWidth * 2,
            y: canvasHeight * 2,
            outerRadius: canvasWidth / 1.7,     //50
            innerRadius: canvasWidth - 35,      //15
            holeRadius: 10,
            numTeeth: 0,//was 8
            theta: 0.14,
            thetaSpeed: 2 / 1000,
            lightColor: "#c4e65a",
            darkColor: "#c4e65a",
            clockwise: true
        }));

        anim.setStage(function(){
            // update
            for (var i = 0; i < gears.length; i++) {
                var gear = gears[i];
                var thetaIncrement = gear.thetaSpeed * this.getTimeInterval();
                gear.theta += gear.clockwise ? thetaIncrement : -1 * thetaIncrement;
            }
            // clear
            this.clear();
            // draw
            for (var i = 0; i < gears.length; i++) {
                gears[i].draw(context);
            }
        });

        if(animated == true) {
            anim.start();
        }
        else if(animated == false) {
            anim.stop();
        }
    };
}

var Animation = function(canvasId){
    this.canvas = document.getElementById(canvasId);
    this.context = this.canvas.getContext("2d");
    this.t = 0;
    this.timeInterval = 0;
    this.startTime = 0;
    this.lastTime = 0;
    this.frame = 0;
    this.animating = false;

    window.requestAnimFrame = (function(callback){
        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback){
            window.setTimeout(callback, 1000 /60);
        };
    })();
};

/**
 * Animation Frames
 */
Animation.prototype.getContext = function(){
    return this.context;
};

Animation.prototype.getCanvas = function(){
    return this.canvas;
};

Animation.prototype.clear = function(){
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
};

Animation.prototype.setStage = function(func){
    this.stage = func;
};

Animation.prototype.isAnimating = function(){
    return this.animating;
};

Animation.prototype.getFrame = function(){
    return this.frame;
};

Animation.prototype.start = function(){
    this.animating = true; 
    var date = new Date();
    this.startTime = date.getTime();
    this.lastTime = this.startTime;
    
    if (this.stage !== undefined) {
        this.stage();
    }
    
    this.animationLoop();
};

Animation.prototype.stop = function(){
    this.animating = false;
};

Animation.prototype.getTimeInterval = function(){
    return this.timeInterval;
};

Animation.prototype.getTime = function(){
    return this.t;
};

Animation.prototype.getFps = function(){
    return this.timeInterval > 0 ? 1000 / this.timeInterval : 0;
};

Animation.prototype.animationLoop = function(){
    var that = this;
    this.frame++;
    var date = new Date();
    var thisTime = date.getTime();
    this.timeInterval = thisTime - this.lastTime;
    this.t += this.timeInterval;
    this.lastTime = thisTime;
    
    if (this.stage !== undefined) {
        this.stage();
    }
    if (this.animating) {
        requestAnimFrame(function(){
            that.animationLoop();
        });
    }
};